import React, { Component } from 'react';
import axios from 'axios';

import AddTournament from './Components/AddTournament/AddTournament';
import AddUser from './Components/AddUser/AddUser';
import AddParticipant from './Components/AddParticipant/AddParticipant';
import AddGame from './Components/AddGame/AddGame';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {};
    this.state.tournaments = [];
    this.state.games = [];
    this.state.covers = [];
    this.state.users = [];
    this.state.tournament_participants = [];
    this.state.gamesDB = [];

    
    // fields for editing a tournament
    this.state.tournament_name = "";
    this.state.id = "";
    this.state.game_id = "";
    this.state.location = "";
    this.state.tournament_date = "";
    this.state.organiser_id = "";


    //fields for editing a user

    this.state.username = "";
    this.state.user_password = "";
    this.state.email = "";
    this.state.user_type = "";



    //fields for editing a tournament participant
    this.state.tournament_id = "";
    this.state.user_id = "";



    //fields for editing a game
    this.state.game_name = "";
    this.state.game_description = "";


  }




  //ADD functions
  onAddTournament = (tournament) => {
    console.log('adding tournament');
    let tournaments = this.state.tournaments;

    tournaments.push(tournament);
    this.setState({
      tournaments: tournaments
    })
  }


  onAddUser = (user) => {
    console.log('adding user');
    let users = this.state.users;

    users.push(user);
    this.setState({
      users: users
    })
  }


  onAddParticipant = (participant) => {
    console.log('adding participant');
    let tournament_participants = this.state.tournament_participants;

    tournament_participants.push(participant);
    this.setState({
      tournament_participants: tournament_participants
    })
  }

  onAddGame = (game) => {
    console.log('adding game');
    let games = this.state.games;

    games.push(game);
    this.setState({
      games: games
    })
  }

  

  //DELETE functions
  deleteTournament(tournament) {
    console.log(tournament);


    axios.delete("http://localhost:4111/delete/tournament/" + tournament.id).then(response => {
      console.log(response)

      const newState = this.state.tournaments.slice();
      newState.splice(newState.indexOf(tournament), 1)
      this.setState({
        tournaments: newState
      })
    }).catch(error => {
      console.log(error)
    })


  }



  deleteUser(user) {
    console.log(user);


    axios.delete("http://localhost:4111/delete/user/" + user.id).then(response => {
      console.log(response)

      const newState = this.state.users.slice();
      newState.splice(newState.indexOf(user), 1)
      this.setState({
        users: newState
      })
    }).catch(error => {
      console.log(error)
    })


  }


  deleteGame(game) {
    console.log(game);


    axios.delete("http://localhost:4111/delete/game/" + game.id).then(response => {
      console.log(response)

      const newState = this.state.games.slice();
      newState.splice(newState.indexOf(game), 1)
      this.setState({
        games: newState
      })
    }).catch(error => {
      console.log(error)
    })


  }
  deleteParticipant(participant) {
    console.log(participant);


    axios.delete("http://localhost:4111/delete/tournament_participant/" + participant.tournament_id).then(response => {
      console.log(response)

      const newState = this.state.tournament_participants.slice();
      newState.splice(newState.indexOf(participant), 1)
      this.setState({
        tournament_participants: newState
      })
    }).catch(error => {
      console.log(error)
    })


  }

  getCoverForGame(gameId) {
    console.log('getting cover for game: ' + gameId);
    for (let i = 0; i < this.state.covers.length; i++) {
      let cover = this.state.covers[i];
      if (cover.id === gameId) {
        console.log('found: ' + cover.url);
        return cover.url;
      }
    }
    return null;
  }


  //EDIT functions - 2 
  //TOURNAMENT

  editTournamentUpdateForm(tournament){
    console.log('setting state', tournament);
    

    this.setState({
      id : tournament.id,
      tournament_name : tournament.tournament_name,
      game_id : tournament.game_id,
      location : tournament.location,
      tournament_date : tournament.tournament_date,
      organiser_id : tournament.organiser_id
    });

  }

  
  //Called when you click on the Edit Tournament button - it updates the data in our database
  editTournament() {
    console.log('edit tournament called');
    
    //1.Getting the values from our form 
    let tournament = {
      id : this.state.id,
      tournament_name: this.state.tournament_name,
      game_id: this.state.game_id,
      location: this.state.location,
      tournament_date: this.state.tournament_date,
      organiser_id: this.state.organiser_id
    }
    
    
    console.log('edit tournament called');
    console.log(tournament);
    
    //2.Updating the data in our database
    axios.put("http://localhost:4111/update/tournament/" + tournament.id, tournament).then(response => {
      console.log(response)

         axios.get("http://localhost:4111/get/all-tournaments").then((response => {
        this.setState({
          tournaments: response.data
        })
      }))

    }).catch(error => {
      console.log(error)
    })




  }



  //EDIT USERS
    editUserUpdateForm(user){
    console.log('setting state', user);
    

    this.setState({
      id: user.id,
      username : user.username,
      user_password : user.user_password,
      email : user.email,
      user_type : user.user_type,
    
    });

  }

  
  //Called when you click on the Edit user button - it updates the data in our database
  editUser() {
    console.log('edit user called');
    
    //1.Getting the values from our form 
    let user = {
      id: this.state.id,
      username: this.state.username,
      user_password: this.state.user_password,
      email: this.state.email,
      user_type: this.state.user_type

    }
    
    
    console.log('edit user called');
    console.log(user);
    
    //2.Updating the data in our database
    axios.put("http://localhost:4111/update/user/" + user.id, user).then(response => {
      console.log(response)

         axios.get("http://localhost:4111/get/all-users").then((response => {
        this.setState({
          users: response.data
        })
      }))

    }).catch(error => {
      console.log(error)
    })




  }

  //EDIT GAME
  editGameUpdateForm(game){
    console.log('setting state', game);
    

    this.setState({
      id : game.id,
      game_name : game.game_name,
      game_description : game.game_description,
      
    });

  }

  
  //Called when you click on the Edit Tournament button - it updates the data in our database
  editGame() {
    console.log('edit game called');
    
    //1.Getting the values from our form 
    let game = {
      id : this.state.id,
      game_name: this.state.game_name,
      game_description: this.state.game_description,
      }
    
    
    console.log('edit game called');
    console.log(game);
    
    //2.Updating the data in our database
    axios.put("http://localhost:4111/update/game/" + game.id, game).then(response => {
      console.log(response)

         axios.get("http://localhost:4111/get/all-games").then((response => {
        this.setState({
          gamesDB: response.data
        })
      }))

    }).catch(error => {
      console.log(error)
    })




  }

  //EDIT PARTICIPANTS

  editParticipantUpdateForm(participant){
    console.log('setting state', participant);
    

    this.setState({
     tournament_id: participant. tournament_id,
       user_id: participant.user_id
    });

  }

  
  //Called when you click on the Edit Tournament button - it updates the data in our database
  editParticipant() {
    console.log('edit participant called');
    
    //1.Getting the values from our form 
    let participant = {
      tournament_id : this.state.tournament_id,
      user_id: this.state.user_id
      }
    
    
    console.log('edit participant called');
    console.log(participant);
    
    //2.Updating the data in our database
    axios.put("http://localhost:4111/update/tournament_participant/" + participant.tournament_id, participant).then(response => {
      console.log(response)

         axios.get("http://localhost:4111/get/all-tournament_participants").then((response => {
        this.setState({
          tournament_participants: response.data
        })
      }))

    }).catch(error => {
      console.log(error)
    })




  }



  //IGDB games fetching
  loadGamesIGDB() {
    let config = {
      headers: {
        "user-key": "a3adfeb756cf7ac6f8ec30a5114f83a5",
        "Content-Type": "text/plain"
      }
    }

    axios.post("https://api-v3.igdb.com/games/", "fields *;", config).then((response => {
      this.setState({
        games: response.data
      })


    }))

    axios.post("https://api-v3.igdb.com/covers/", "fields *;", config).then((response => {
      this.setState({
        covers: response.data
      })
    }))

  }

  componentWillMount() {

    this.loadGamesIGDB();

    axios.get("http://localhost:4111/get/all-tournaments").then((response => {
      this.setState({
        tournaments: response.data
      })
    }))

    axios.get("http://localhost:4111/get/all-users").then((response => {
      this.setState({
        users: response.data
      })
    }))

    axios.get("http://localhost:4111/get/all-tournament_participants").then((response => {
      this.setState({
        tournament_participants: response.data
      })
    }))

    axios.get("http://localhost:4111/get/all-games").then((response => {
      this.setState({
        gamesDB: response.data
      })
    }))

  }

 // Edit form update tournament


 //Functions for the EDIT TOURNAMENT FORM
 handleChangeTournamentName = (event) =>{
  this.setState({
      tournament_name: event.target.value
  })

  console.log(this.state.tournament_name);
}


hangleChangeTournamentId = (event) =>{
  this.setState({
      id: event.target.value
  })

  console.log(this.state.id);
}



handleChangeGameId = (event) =>{
  this.setState({
      game_id: event.target.value
  })

  console.log(this.state.game_id);
}

handleChangeLocation = (event) =>{
  this.setState({
      location: event.target.value
  })
}


handleChangeTournamentDate = (event) =>{
  this.setState({
      tournament_date: event.target.value
  })
} 

handleChangeOrganiserId = (event) =>{
  this.setState({
      organiser_id: event.target.value
  })
}


//EDIT USER FORM
handleChangeUserId = (event) =>{
        this.setState({
            user_id: event.target.value
        })

        console.log(this.state.user_id);
    }

handleChangeUserName = (event) =>{
        this.setState({
            username: event.target.value
        })

        console.log(this.state.username);
    }

    handleChangeUserPassword = (event) =>{
        this.setState({
            user_password: event.target.value
        })

        console.log(this.state.user_password);
    }

    handleChangeEmail = (event) =>{
        this.setState({
            email: event.target.value
        })
        console.log(this.state.email);
    }


    handleChangeUserType = (event) =>{
        this.setState({
            user_type: event.target.value
        })
        console.log(this.state.user_type);
    } 


//EDIT GAME FORM
  handleChangeGameName= (event) =>{
        this.setState({
            game_name: event.target.value
        })

        console.log(this.state.game_name);
    }

    handleChangeGameDescription= (event) =>{
        this.setState({
            game_description: event.target.value
        })

        console.log(this.state.game_description);
    }


//edit participants form
handleChangeTournamentIdp = (event) =>{
  this.setState({
      tournament_id: event.target.value
  })

  console.log(this.state.tournament_id);
}
handleChangeUserIdp = (event) =>{
  this.setState({
      user_id: event.target.value
  })

  console.log(this.state.user_id);
}
  render() {





    let tournaments = this.state.tournaments.map((tournament) => {
      return (
        <tr key={tournament.id}>

          <td>{tournament.id}</td>    <span>&nbsp;&nbsp;</span>
          <td>
            {tournament.tournament_name}

          </td>
          <span>&nbsp;&nbsp;</span>   <td>{tournament.game_id}</td>
          <span>&nbsp;&nbsp;</span> <td>{tournament.location}</td>  <span>&nbsp;&nbsp;</span>  <td>{tournament.tournament_date}</td> <span>&nbsp;&nbsp;</span> <td>{tournament.organiser_id}</td>
          <td>
            <button type="button" class="btn btn-outline-success"
              onClick={this.editTournamentUpdateForm.bind(this, tournament)}
              >Edit
            </button>
            <button type="button" class="btn btn-outline-danger" onClick={this.deleteTournament.bind(this, tournament)}>Delete</button>
          </td>
        </tr>
      )
    })



    let users = this.state.users.map((user) => {
      return (
        <tr key={user.id}>

          <td>{user.id}</td>    <span>&nbsp;&nbsp;</span>   <td>{user.username}</td>   <span>&nbsp;&nbsp;</span>   <td>{user.user_password}</td>
          <span>&nbsp;&nbsp;</span> <td>{user.email}</td>  <span>&nbsp;&nbsp;</span>  <td>{user.user_type}</td>
          <td><button type="button" class="btn btn-outline-success"
          onClick={this.editUserUpdateForm.bind(this, user)}>Edit</button> <button type="button" class="btn btn-outline-danger" onClick={this.deleteUser.bind(this, user)}>Delete</button></td>
        </tr>
      )
    })


    let tournamentParticipants = this.state.tournament_participants.map((tournament_participant) => {
      return (
        <tr key={tournament_participant.tournament_id}>

          <td>{tournament_participant.tournament_id}</td>    <span>&nbsp;&nbsp;</span>   <td>{tournament_participant.user_id}</td>   <span>&nbsp;&nbsp;</span>
          <td><button type="button" class="btn btn-outline-success"
          onClick={this.editParticipantUpdateForm.bind(this, tournament_participant)}>Edit</button> <button type="button" class="btn btn-outline-danger" onClick={this.deleteParticipant.bind(this,tournament_participant)}>Delete</button></td>
        </tr>
      )
    })


    let gamesDB = this.state.gamesDB.map((game) => {
      return (
        <tr key={game.id}>

          <td>{game.id}</td>    <span>&nbsp;&nbsp;</span>   <td>{game.game_name}</td>   <span>&nbsp;&nbsp;</span>   <td>{game.game_description}</td>
          <td><button type="button" class="btn btn-outline-success"
           onClick={this.editGameUpdateForm.bind(this, game)}>Edit</button> <button type="button" class="btn btn-outline-danger" onClick={this.deleteGame.bind(this, game)}>Delete</button></td>
        </tr>
      )
    })


    return (


    


      <div className="App" style={{ background: "#e6e6e6" }} >

        <html>
          <head>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
            </link>
          </head>
        </html>

        <img src="https://upload.wikimedia.org/wikipedia/en/thumb/e/e0/WPVG_icon_2016.svg/1024px-WPVG_icon_2016.svg.png" height="50" width="50">
        </img>
        <h2 class="font-weight-bolder">VIDEOGAME TOURNAMENT ORGANISER</h2>
        <h2 style={{ color: "#1D4D6F" }}>-TOURNAMENTS</h2>



        <div>

        
            <h2>Edit a tournament</h2>
            <input type="text"  onChange={this.hangleChangeTournamentId}
            value={this.state.id}/>


            <input type="text" placeholder="Tournament name" onChange={this.handleChangeTournamentName}
            value={this.state.tournament_name}/>

            <input type="text" placeholder="Game id" onChange={this.handleChangeGameId}
            value={this.state.game_id}/>

            <input type="text" placeholder="Location" onChange={this.handleChangeLocation}
            value={this.state.location}/>

            <input type="text" placeholder="Tournament date" onChange={this.handleChangeTournamentDate}
            value={this.state.tournament_date}/>

            <input type="text" placeholder="Organiser id" onChange={this.handleChangeOrganiserId}
            value={this.state.organiser_id}/>

            <button class="btn btn-light" onClick={this.editTournament.bind(this)}>Update tournament</button>
          
        </div>


        <table>
          <thead><tr><th>ID</th> <span>&thinsp;</span>   <th>NAME</th> <span>&nbsp;&nbsp;</span>  <th>GAME ID</th> <span>&nbsp;&nbsp;</span>  <th> LOCATION</th> <span>&nbsp;&nbsp;</span>  <th>DATE</th> <span>&nbsp;&nbsp;</span>  <th>ORGANISER ID</th>
            <th>ACTIONS</th>

          </tr></thead>
          <tbody>

            {tournaments}

          </tbody>
        </table>

        <AddTournament tournamentAdded={this.onAddTournament} />

        <div style={{ marginTop: '50px' }} />

        <h2 style={{ color: "#1D4D6F" }}>-USERS</h2>


 <div>

        
            <h2>Edit a user</h2>
        

            <input type="text"  onChange={this.hangleChangeUserId}
            value={this.state.user_id}/>
            <input type="text" placeholder="User name" onChange={this.handleChangeUserName}
            value={this.state.username}/>

            <input type="text" placeholder="User PASSWORD" onChange={this.handleChangeUserPassword}
            value={this.state.user_password}/>

            <input type="text" placeholder="Email" onChange={this.handleChangeEmail}
            value={this.state.email}/>

            <input type="text" placeholder="User type" onChange={this.handleChangeUserType}
            value={this.state.user_type}/>

         

            <button class="btn btn-light" onClick={this.editUser.bind(this)}>Update user</button>
          
        </div>





        <table>
          <thead><tr><th>ID</th> <span>&thinsp;</span>   <th>USER NAME</th> <span>&nbsp;&nbsp;</span>  <th>USER PASSWORD</th> <span>&nbsp;&nbsp;</span>  <th>E-MAIL</th> <span>&nbsp;&nbsp;</span>  <th>USER TYPE</th>
            <th>ACTIONS</th>
            <th></th>
          </tr></thead>
          <tbody>

            {users}

          </tbody>
        </table>
        <AddUser userAdded={this.onAddUser} />


        <div style={{ marginTop: '50px' }} />

        <h2 style={{ color: "#1D4D6F" }}>-TOURNAMENT PARTICIPANTS</h2>
        <div>
            <h2>Edit a participant</h2>
        

            <input type="text" placeholder="Tournament id" onChange={this.handleChangeTournamentIdp}
            value={this.state.tournament_id}/>

            <input type="text" placeholder="User id " onChange={this.handleChangeUserIdp}
            value={this.state.user_id}/>

    
         

            <button class="btn btn-light" onClick={this.editParticipant.bind(this)}>Update participant</button>
          
        </div>
        <table>
          <thead><tr><th>TOURNAMENT ID</th> <span>&thinsp;</span>   <th>USER ID</th> <span>&nbsp;&nbsp;</span>
            <th>ACTIONS</th>
            <th></th>
          </tr></thead>
          <tbody>

            {tournamentParticipants}

          </tbody>
        </table>
        <AddParticipant participantAdded={this.onAddParticipant} />
        <div style={{ color: "#1D4D6F" }} />
        
        <div style={{ marginTop: '50px' }} />


        <h2 style={{ color: "#1D4D6F" }}>-GAMES</h2>




<div>
            <h2>Edit a game</h2>
        

            <input type="text" placeholder="Game name" onChange={this.handleChangeGameName}
            value={this.state.game_name}/>

            <input type="text" placeholder="Game description" onChange={this.handleChangeGameDescription}
            value={this.state.game_description}/>

    
         

            <button class="btn btn-light" onClick={this.editGame.bind(this)}>Update game</button>
          
        </div>




        <table >
          <thead><tr><th>ID</th> <span>&thinsp;</span>   <th>NAME</th> <span></span>  <th>DESCRIPTION</th>
            <th>ACTIONS</th>
            <th></th>
          </tr></thead>
          <tbody>

            {gamesDB}

          </tbody>
        </table>
        <AddGame gameAdded={this.onAddGame} />
        <div style={{ marginTop: '80px' }} />
  <br />


        <h2>IGDB GAMES</h2>
        <div style={{ marginTop: '30px' }}>
          {this.state.games.map(game =>
            <div key={game.id} style={{
              border: '1px solid black',
              height: '150px'
            }}>
              <span style={{ fontFamily: "Arial", fontSize: 'x-large', paddingLeft: '10px' }}>
                {game.name}
              </span>
              <br />
              <span style={{ fontFamily: "Arial", paddingLeft: '30px' }}>{game.summary}</span>

              <img src={this.getCoverForGame(game.cover)} ></img>
            </div>

          )}
        </div>






      </div>
    );
  }
}

export default App;