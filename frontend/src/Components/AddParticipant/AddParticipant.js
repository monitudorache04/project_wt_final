import React, { Component } from 'react';
import axios from 'axios';



class AddParticipant extends Component {
   
    //Constructor
    constructor(props){
        super(props);
        this.state = {}; 
        this.state.tournament_id = "";
        this.state.user_id = "";
     
    }

    //Creating the functions

    handleChangeTournamentId = (event) =>{
        this.setState({
            tournament_id: event.target.value
        })

        console.log(this.state.tournament_id);
    }

    handleChangeUserId= (event) =>{
        this.setState({
            user_id: event.target.value
        })

        console.log(this.state.user_id);
    }





    handleAddClick = () =>{
        console.log('handleAddClick adding tournament participant ');
        //If someone presses the add user button 
        //1.A new participant object will be created
        let participant = {
            tournament_id: this.state.tournament_id,
            user_id: this.state.user_id
          

        }
        
        //2.Adding it to the database with axios post!!!
        axios.post("http://localhost:4111/add/tournament_participant", participant).then((response)=>{
            if(response.status === 200){

                


                this.props.participantAdded(participant)
            }
        }).catch((err)=>{
            console.log("NU MERGE!");
        })

       
    }


  render() {
//tournament id
//user id 

   
    //Building the form for adding a tournament
    return (
        <div>
            <h2>Add a participant </h2>
            <input type="text" placeholder="Tournament id " onChange={this.handleChangeTournamentId}
            value={this.state.tournament_id}/>

            <input type="text" placeholder="User id" onChange={this.handleChangeUserId}
            value={this.state.user_id}/>


         
            <button class="btn btn-light" onClick={this.handleAddClick}>Add participant</button>

        </div>
    
    );
  }
}


export default AddParticipant;
