import React, { Component } from 'react';
import axios from 'axios';



class AddTournament extends Component {
   
    //Constructor
    constructor(props){
        super(props);

     



        this.state = {}; 
        this.state.tournament_name = "";
        this.state.game_id = "";
        this.state.location = "";
        this.state.tournament_date = "";
        this.state.organiser_id = "";
    }





    //Creating the functions

    handleChangeTournamentName = (event) =>{
        this.setState({
            tournament_name: event.target.value
        })

        console.log(this.state.tournament_name);
    }

    handleChangeGameId = (event) =>{
        this.setState({
            game_id: event.target.value
        })

        console.log(this.state.game_id);
    }

    handleChangeLocation = (event) =>{
        this.setState({
            location: event.target.value
        })
    }


    handleChangeTournamentDate = (event) =>{
        this.setState({
            tournament_date: event.target.value
        })
    } 

    handleChangeOrganiserId = (event) =>{
        this.setState({
            organiser_id: event.target.value
        })
    }

    handleAddClick = () =>{
        console.log('handleAddClick adding tournament');
        //If someone presses the add tournament button 
        //1.A new tournament object will be created
        let tournament = {
            tournament_name: this.state.tournament_name,
            game_id: this.state.game_id,
            location:this.state.location,
            tournament_date:this.state.tournament_date,
            organiser_id:this.state.organiser_id
        }
        
        //2.Adding it to the database with axios post!!!
        axios.post("http://localhost:4111/add/tournament", tournament).then((response)=>{
            if(response.status === 200){

                


                this.props.tournamentAdded(tournament)
            }
        }).catch((err)=>{
            console.log("NU MERGE!");
        })

       
    }













  render() {

    //Tournament name
    //Game id
    //Location
    //Tournament date
    //Organiser id 

   
    //Building the form for adding a tournament
    return (
        <div>
            <h2>Create a tournament! :)</h2>
            <input type="text" placeholder="Tournament name" onChange={this.handleChangeTournamentName}
            value={this.state.tournament_name}/>

            <input type="text" placeholder="Game id" onChange={this.handleChangeGameId}
            value={this.state.game_id}/>

            <input type="text" placeholder="Location" onChange={this.handleChangeLocation}
            value={this.state.location}/>

            <input type="text" placeholder="Tournament date" onChange={this.handleChangeTournamentDate}
            value={this.state.tournament_date}/>

            <input type="text" placeholder="Organiser id" onChange={this.handleChangeOrganiserId}
            value={this.state.organiser_id}/>

            <button class="btn btn-light" onClick={this.handleAddClick}>Add tournament</button>
          
        </div>
    
    );
  }
}


export default AddTournament;
