import React, { Component } from 'react';
import axios from 'axios';



class AddUser extends Component {
   
    //Constructor
    constructor(props){
        super(props);
        this.state = {}; 
        this.state.username = "";
        this.state.user_password = "";
        this.state.email = "";
        this.state.user_type = "";
    }

    //Creating the functions

    handleChangeUserName = (event) =>{
        this.setState({
            username: event.target.value
        })

        console.log(this.state.username);
    }

    handleChangeUserPassword = (event) =>{
        this.setState({
            user_password: event.target.value
        })

        console.log(this.state.user_password);
    }

    handleChangeEmail = (event) =>{
        this.setState({
            email: event.target.value
        })
        console.log(this.state.email);
    }


    handleChangeUserType = (event) =>{
        this.setState({
            user_type: event.target.value
        })
        console.log(this.state.user_type);
    } 





    handleAddClick = () =>{
        console.log('handleAddClick adding user');
        //If someone presses the add user button 
        //1.A new user object will be created
        let user = {
            username: this.state.username,
            user_password: this.state.user_password,
            email:this.state.email,
            user_type:this.state.user_type,
   
        }
        
        //2.Adding it to the database with axios post!!!
        axios.post("http://localhost:4111/add/user", user).then((response)=>{
            if(response.status === 200){

                


                this.props.userAdded(user)
            }
        }).catch((err)=>{
            console.log("NU MERGE!");
        })

       
    }


  render() {
//username
//user_password
//email
//user_type

   
    //Building the form for adding a user
    return (
        <div>
            <h2>Add a user</h2>
            <input type="text" placeholder="User name" onChange={this.handleChangeUserName}
            value={this.state.username}/>

            <input type="text" placeholder="User password" onChange={this.handleChangeUserPassword}
            value={this.state.user_password}/>

            <input type="text" placeholder="Email" onChange={this.handleChangeEmail}
            value={this.state.email}/>

            <input type="text" placeholder="User type" onChange={this.handleChangeUserType}
            value={this.state.user_type}/>

         
            <button class="btn btn-light" onClick={this.handleAddClick}>Add user</button>

        </div>
    
    );
  }
}


export default AddUser;
