import React, { Component } from 'react';
import axios from 'axios';



class AddGame extends Component {
   
    //Constructor
    constructor(props){
        super(props);
        this.state = {}; 
        this.state.game_name = "";
        this.state.game_description = "";
     
    }

    //Creating the functions

    handleChangeGameName= (event) =>{
        this.setState({
            game_name: event.target.value
        })

        console.log(this.state.game_name);
    }

    handleChangeGameDescription= (event) =>{
        this.setState({
            game_description: event.target.value
        })

        console.log(this.state.game_description);
    }





    handleAddClick = () =>{
        console.log('handleAddClick adding game');
        //If someone presses the add user button 
        //1.A new game object will be created
        let game = {
            game_name: this.state.game_name,
            game_description: this.state.game_description
          

        }
        
        //2.Adding it to the database with axios post!!!
        axios.post("http://localhost:4111/add/game", game).then((response)=>{
            if(response.status === 200){
                this.props.gameAdded(game)
            }
        }).catch((err)=>{
            console.log("NU MERGE!");
        })

       
    }


  render() {
//game name
//game description
   
    //Building the form for adding a game
    return (
        <div>
            <h2>Add a game </h2>
            <input type="text" placeholder="Game name " onChange={this.handleChangeGameName}
            value={this.state.game_name}/>

            <input type="text" placeholder="Game description" onChange={this.handleChangeGameDescription}
            value={this.state.game_description}/>


         
            <button class="btn btn-light" onClick={this.handleAddClick}>Add game</button>

        </div>
    
    );
  }
}


export default AddGame;
